const input = document.createElement('input');
input.placeholder = 'Price';
document.body.append(input);
input.type = 'number';

input.addEventListener('focus', () => {
    input.classList.add('active');
    const p = document.querySelector('p');
    if (p) {
        p.remove();
        input.classList.remove('error');
    }
});

input.addEventListener('blur', () => {
    const value = input.value;
    input.style.color = "green";
    if (value === '') {
        input.classList.remove('active');
    } else if (value < 0) {
        input.classList.remove('active');
        input.classList.add('error');
        const p = document.createElement('p');
        p.innerText = 'Please enter correct price.';
        input.after(p);
        input.value = '';
    } else {
        const span = document.createElement('span');
        const button = document.createElement('button');
        button.innerText = 'x';
        span.innerText = `Текущая цена: ${value}`;
        span.append(button);
        input.before(span);
        button.addEventListener('click', () => removeElement(span))
    }

});
function removeElement (span) {
    span.remove();
    input.value = '';
}
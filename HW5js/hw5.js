function CreateNewUser () {
    this.firstName = prompt("Введіть ваше ім'я");
    this.lastName = prompt("Введіть ваше прізвище");
    this.birthday = prompt("Введіть дату вашого народження", "dd.mm.yyyy");
     //this.age = age;
    this.getLogin = function () {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    }
    this.getAge = function () {
        let today = new Date();
        let userBirthday = Date.parse(`${this.birthday.slice(6)}-${this.birthday.slice(3, 5)}-${this.birthday.slice(0, 2)}`);
        let age = ((today - userBirthday) / (1000 * 60 * 60 * 24 * 30 * 12)).toFixed(0);
        if (age < today) {
            return `Вам ${age - 1} років`;
        } else {
            return `Вам ${age} років`;
        }
    };
    this.getPassword =() => {
        return `${this.firstName[0].toUpperCase()}${this.lastName.toLocaleLowerCase()}${this.birthday.split('.')[2]}`;
    }
}

let newUser = new CreateNewUser();
console.log(newUser.getAge());
console.log('Ваш логин: ' + newUser.getLogin());
console.log('Ваш пароль: ' + newUser.getPassword());

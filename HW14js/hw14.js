$.fn.extend({
    toggleText: function (a, b) {
        return this.text(this.text() == b ? a : b);
    },
});

$(".top-rated").append($("<button>", { text: "Свернуть", class: "toggleButton" }));
$("button").click(function () {
    $(".top-rated-photos").slideToggle("slow");
    $(".toggleButton").toggleText("Свернуть", "Развернуть");
});
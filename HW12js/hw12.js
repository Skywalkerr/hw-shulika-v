const imageWrapper = document.querySelectorAll(".image-to-show");
const resetDisplay = () => {
  for (const img of imageWrapper) {
    img.style.display = "none";
    imageWrapper[0].style.display = "block";
  }
};
resetDisplay();

let count = 1;
function showImages() {
  if (count < 4) {
    imageWrapper[count].style.display = "block";
  }
  if (count > 0) {
    imageWrapper[count - 1].style.display = "none";
  }
  count++;
  if (count > 4) {
    resetDisplay();
    count = 1;
  }
}

let start = setInterval(showImages, 3000);

const cancelBtn = document.createElement("button");
const resumeBtn = document.createElement("button");
cancelBtn.innerText = "Прекратить";
resumeBtn.innerText = "Продолжить";
document.body.append(cancelBtn);
document.body.append(resumeBtn);
cancelBtn.addEventListener("click", () => {
  clearInterval(start);
});

resumeBtn.addEventListener("click", () => {
  start = setInterval(showImages, 3000);
});
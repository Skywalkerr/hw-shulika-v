const tabMenu = document.querySelector(".tabs");

tabMenu.addEventListener("click", (event) => {
    if (event.target.tagName === "LI") {
        change(event);
    }
});
const change = (event) => {
    let menuItems = document.querySelectorAll(".tabs-title");
    for (const item of menuItems) {
        if (item.classList.contains("active")) {
            item.classList.remove("active");
        }
    }
    if (!event.target.classList.contains("active")) {
        event.target.classList.add("active");
    }
    let id = event.target.innerHTML.toLowerCase();
    console.log(id);
    let contentItems = document.querySelectorAll(".content-item");
    for (const item of contentItems) {
        if (item.classList.contains("active")) {
            item.classList.remove("active");
        }
    }
    if (!document.getElementById(id).classList.contains("active")) {
        document.getElementById(id).classList.add("active");
    }
};

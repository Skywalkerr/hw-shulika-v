function switcherTheme() {
    const html = document.firstElementChild;
    const button = document.querySelector("button");

    const setTheme = (theme = "light") => {
        html.setAttribute("color-scheme", theme);
        localStorage.setItem("Theme", ${theme});
    };

    button.addEventListener("click", () => {
        if (html.getAttribute("color-scheme") === "light") {
            setTheme("dark");
        } else {
            setTheme("light");
        }
    });

    document.addEventListener("DOMContentLoaded", function () {
        const theme = localStorage.getItem("Theme");
        setTheme(theme);
    });
}

switcherTheme();